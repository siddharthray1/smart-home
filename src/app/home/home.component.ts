import { Component, OnInit } from '@angular/core';
import { SmartHomeService } from '../service/smart-home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private fetchBill: SmartHomeService) { }

  ngOnInit() {
  }

  onDevicesSelected(value: any) {
    console.log('selected items', value);
    const deviceObj = {
      tv: value.tv || false,
      refrigerator: value.refrigerator || false,
      wifi: value.wifi || false,
      bulb: value.bulb || false

    }
    this.fetchBill.fetchBill(deviceObj)
      .subscribe(data => console.log(data));
  }

}
