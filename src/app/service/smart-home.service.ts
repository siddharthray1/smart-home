import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SmartHomeService {
  urls = environment;

  constructor(private http: HttpClient) { }

  fetchBill(deviceObj) {
  let params = new HttpParams();
  params = deviceObj;
    return this.http.get(this.urls.getBillurl, {params: params});
  }

  fetchDeviceStatus() {
    return this.http.get(this.urls.getDevicesStatusUrl);
  }


}
